import matplotlib.pyplot as plt
import networkx as nx

G = nx.DiGraph()

G.add_node('a', demand=-5)
G.add_node('d', demand=5)
G.add_edge('a', 'b', weight=3, capacity=4)
G.add_edge('a', 'c', weight=6, capacity=10)
G.add_edge('b', 'd', weight=1, capacity=9)
G.add_edge('c', 'd', weight=2, capacity=5)
flowCost, flowDict = nx.network_simplex(G)

print("flowcost: %s" % flowCost)
print("flowdict: %s" % flowDict)

resGraph = nx.DiGraph()

for key, value in flowDict.items():
    resGraph.add_node(key)
    for key2, v in value.items():
        resGraph.add_edge(key, key2, weight=v)
        print("from node: %s to node: %s with capacity %s" % (key, key2, v))

plt.subplot(121)
pos = nx.spring_layout(resGraph)
nx.draw_networkx_nodes(resGraph, pos)
nx.draw_networkx_edges(resGraph, pos)
nx.draw_networkx_labels(resGraph, pos)
plt.axis('off')
plt.show()
